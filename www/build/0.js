webpackJsonp([0],{

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductPageModule", function() { return ProductPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__product__ = __webpack_require__(282);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProductPageModule = /** @class */ (function () {
    function ProductPageModule() {
    }
    ProductPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__product__["a" /* ProductPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__product__["a" /* ProductPage */]),
            ],
        })
    ], ProductPageModule);
    return ProductPageModule;
}());

//# sourceMappingURL=product.module.js.map

/***/ }),

/***/ 282:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(28);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ProductPage = /** @class */ (function () {
    function ProductPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.cart = [];
        this.addCheckout = [];
        this.qty = 1;
    }
    ProductPage.prototype.ngOnInit = function () {
        this.product = this.navParams.get('item');
        console.log("product details----", this.product);
    };
    ProductPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ProductPage');
    };
    ProductPage.prototype.incrementQty = function () {
        this.qty += 1;
    };
    ProductPage.prototype.decrementQty = function () {
        if (this.qty - 1 < 1) {
            this.qty = 1;
        }
        else {
            this.qty -= 1;
        }
    };
    ProductPage.prototype.addCart = function (item, q) {
        this.cart.push(item);
        this.cart.push(q);
        console.log("card data...", this.cart);
    };
    ProductPage.prototype.checkout = function (product, qty) {
        this.addCheckout.push(product);
        this.addCheckout.push(qty);
        console.log("checkout data...", this.addCheckout);
    };
    ProductPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-product',template:/*ion-inline-start:"D:\ionic\Other_ionic_appps\myApp\src\pages\product\product.html"*/'<!--\n  Generated template for the ProductPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar class="nav-clr">\n    <ion-title>Product</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding class="home-background">\n  <div>\n    <ion-card class="card-img">\n      <img src="./assets/products/{{product.productImgUrl}}" class="more-img style" /> <br>\n    </ion-card>\n    \n    \n      <h3>{{product.productName}}</h3>\n      <div class="btn-rate">  \n          <button ion-button class="rating">{{product.rating}} &nbsp; <ion-icon name="star"></ion-icon></button>\n        </div>\n      <div class="price"> <b>₹ Price : {{product.price}}</b></div>\n    \n\n    <p ion-text class="txt-style"><b>Description :</b> {{product.descriptions}} </p>\n    <b>Quantity :</b>\n    <button clear (click)="decrementQty()">\n      <ion-icon name="remove-circle"></ion-icon>\n    </button>{{qty}}\n    <button clear (click)="incrementQty()">\n      <ion-icon name="add-circle"></ion-icon>\n    </button>\n\n    <br><br>\n    <button ion-button full class="btn-cart" (click)="addCart(product,qty)">\n      <ion-icon name="cart"></ion-icon>Add to cart\n    </button><button ion-button full class="btn-buy" (click)="checkout(product,qty)">BUY NOW</button>\n    <hr>\n\n  </div>\n</ion-content>'/*ion-inline-end:"D:\ionic\Other_ionic_appps\myApp\src\pages\product\product.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]) === "function" && _b || Object])
    ], ProductPage);
    return ProductPage;
    var _a, _b;
}());

//# sourceMappingURL=product.js.map

/***/ })

});
//# sourceMappingURL=0.js.map