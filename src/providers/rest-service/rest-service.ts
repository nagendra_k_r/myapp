import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

/*
  Generated class for the RestServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestServiceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello RestServiceProvider Provider');
  }
  getProducts() {
    console.log("---called----")
    return new Promise(resolve => {

      this.http.get('http://demo3085351.mockable.io/api/product.json')
        .subscribe(productInfo => {
         
          resolve(productInfo);
        });
    });
  }

}
