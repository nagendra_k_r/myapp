import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage implements OnInit {
  product: any;
  qty: any;
  cart = [];
  addCheckout = [];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.qty = 1;

  }
  ngOnInit() {
    this.product = this.navParams.get('item');
    console.log("product details----", this.product);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductPage');

  }
  incrementQty() {
    this.qty += 1;
  }


  decrementQty() {
    if (this.qty - 1 < 1) {
      this.qty = 1
    } else {
      this.qty -= 1;
    }
  }

  addCart(item, q) {
    this.cart.push(item);
    this.cart.push(q);
    console.log("card data...", this.cart);

  }
  checkout(product, qty) {
    this.addCheckout.push(product);
    this.addCheckout.push(qty);
    console.log("checkout data...", this.addCheckout);
  }

}
