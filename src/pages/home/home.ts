import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RestServiceProvider } from '../../providers/rest-service/rest-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  product: String[];
  advantage: any;
  constructor(public navCtrl: NavController, private restService: RestServiceProvider) {

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');


  }
  ngOnInit() {
    this.restService.getProducts().then((data) => {
      this.product = data as string[];

    });
  }
  goToProductPage(data) {
    this.navCtrl.push('ProductPage', { item: data });
  }

}
