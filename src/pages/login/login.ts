import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, LoadingController, Loading, ToastController } from 'ionic-angular';

import { HomePage } from '../home/home';
// import { FormControl } from '@angular/forms';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  account = {
    fullname: '',
    mobile: '',
    email: '',
    password: '',
    confirm_password: ''
  };
  loading: Loading;
  id: any;
  createSuccess: boolean = false;
  // Our translated text strings
  private loginErrorString: string;
  private opt: string = 'signin';

  constructor(public navCtrl: NavController,
    public nav: NavController,

    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    public forgotCtrl: AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  signin() {

    let userInfo = JSON.parse(localStorage.getItem("user"));
    console.log('userInfo--', userInfo);

    if (this.account.email == '' || this.account.password == '') {
      this.showPopup("Error", "Please enter credintials");
    }
    else if (userInfo) {
      if (this.account.email != userInfo.email) {
        this.showPopup("Error", "Invalid Email address!");
      }
      else if (this.account.password != userInfo.password) {
        this.showPopup("Error", "Invalid Password!");
      }
      else if (this.account.email == userInfo.email && this.account.password == userInfo.password) {
        this.showLoading();
        this.nav.setRoot(HomePage);
      }
    }
    else {
      this.showPopup("Error", "User has not registered!");
    }





  }

  signup() {

    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let result = re.test(this.account.email);
    //email validation
    //let e=this.myform.controls.email.valid;
    if (this.account.mobile.length != 10) {
      this.showPopup("Error", "The Mobile number invalid!");
    }
    else if (!result) {
      this.showPopup("Error", "Invalid Email address!");
    }
    else if (this.account.password.length < 6) {
      this.showPopup("Error", 'The password must 6 character!');

    }
    else if (this.account.password != this.account.confirm_password) {
      this.showPopup("Error", 'The password confirmation does not match.');
    }
    else {
      this.showPopup("Success", 'Account Created.');
      localStorage.setItem("user", JSON.stringify(this.account));
      this.createSuccess = true;

    }


  }

  showPopup(title, text) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: [
        {
          text: 'OK',
          handler: data => {
            if (this.createSuccess) {
              this.opt = 'signin';
              this.nav.popToRoot();

            }
          }
        }
      ]
    });
    alert.present();
  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...',
      dismissOnPageChange: true
    });
    this.loading.present();
  }

  showError(text) {
    this.loading.dismiss();

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present();
  }

}
